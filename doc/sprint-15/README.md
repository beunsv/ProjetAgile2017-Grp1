# Rétrospective de sprint

Nom du scrum master du sprint :  DELPORTE Allan

## Ce que nous avons fait durant ce sprint
Une bonne partie de la bdd a été effectuée.
Debuggage des pages TP commencé.
Changement de salle pour développer l'application android.

## Ce que nous allons faire durant le prochain sprint
Finir la bdd, debbug les page TP, commencer à lier la bdd au site web.
Continuer l'avancement de l'application android.

## PDCA 
### Qu'avons nous testé durant ce sprint ? 
Aucun tests, nos efforts étaient concentrés sur l'application mobile, la reconception et le debbugage.

### Qu'avons nous observé ? 
Il sera difficile de rendre dans les temps l'intégralité des fonctionnalités espérées.
On rencontre des difficulté avec Android Studio.

### Quelle décision prenons nous suite à cette expérience ? 
Faire des choix sur les fonctionnalités prioritaires ; on a exclu l'idée de convertir les TP de .md à html.
Résoudre les difficultés avec Android Studio ou trouver des alternatives rapidement.

### Qu'allons nous tester durant les 2 prochaines heures ? 
la bdd qui sera instaurée, la page TP qui devrait être de retour fonctionnel.
L'application Android.

### À quoi verra-t-on que celà à fonctionné ?
La récupération des informations pour la base de donnée depuis le site web, et directement sur la page TP pour cette dernière.
L'application Android devrait démarrer normalement.